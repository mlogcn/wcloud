/**
 * Created by hardware on 2015/9/28.
 */
'use strict';
var opt = {
  start: [116.4010, 39.9814],
  positionShift: [0, 0],
  testDuring: ['2015090100', '2015090900']
};
var conf = {
  test: true,
  urlDescription: 'mlog weather cloud system',
  pointDays: 7,
  /*server URLs*/
  apiHost: 'http://120.132.57.114/',
  pServer: 'http://120.132.57.114/pdata',
  iServer: 'http://120.132.57.114/metadata',
  /*marker config*/
  markerIndex: 99,
  imagesOpacity: 0.4
};
var t = function (s) {
  console.debug(s && s.stack ? s + '\n' + s.stack : s);
};
var map = new AMap.Map('mapContainer', {
  resizeEnable: true,
  level: 7
});
var position = new AMap.LngLat(opt.start[0], opt.start[1]);
var marker = new AMap.Marker({
  map: map,
  icon: new AMap.Icon({
    size: new AMap.Size(65, 82),
    image: './images/pin.png'
  }),
  offset: new AMap.Pixel(-30, -62),
  zIndex: conf.markerIndex,
  draggable: false
});

var weather = {
  type: 'pre',
  con: 'rain',
  unit: 'mm',
  length: 40,
  value: {}
};
var utils = {
  zeroPad: function (n, width) {
    return new Array(Math.max(width - n.toString().length, 0) + 1).join('0') + n.toString();
  },
  format: function (date, paten) {
    var y = date.getUTCFullYear(), m = utils.zeroPad(date.getUTCMonth() + 1, 2), d = utils.zeroPad(date.getUTCDate(), 2), h = utils.zeroPad(date.getUTCHours(), 2);
    switch (paten) {
      case 'mm/dd':
        return m + '/' + d;
      case 'ymd':
        return y + m + d;
      case 'full':
      default :
        return y + m + d + h;
    }
  },
  addDate: function (start, days) {
    return utils.format(new Date(start.getTime() + days * 1000 * 3600 * 24));
  },
  rep: function () {
    var t = arguments, e = t[0] || '', a = 1;
    for (var i = t.length; i > a; a++) {
      e = e.replace(/%s/, t[a]);
    }
    return e;
  },
  mAjax: function (url, cb) {
    $.ajax({
      url: url,
      type: 'GET',
      dataType: 'json',
      ifModified: true,
      contentType: 'application/x-www-form-urlencoded;charset=utf-8',
      success: cb
    });
  },
  reUrl: function (pos) {
    history.pushState({}, conf.urlDescription, document.URL.split('#')[0] + '#' + pos.lng.toFixed(4) + ',' + pos.lat.toFixed(4));
  }
};

var weatherTools = {
  factory: function (type, con) {
    return weather = {
      type: type,
      con: con,
      length: 40
    };
  },
  format: function (d) {
    var w = '';
    if (typeof d[weather.type] !== 'undefined' && d[weather.type].length > 0) {
      switch (weather.type) {
        case 'rain':
          w = (d.rain[0].split(':')[1] * 1000).toFixed(1);
          return w === 0 ? 0 : (w + '<br>mm');
        case 'temp':
          return Math.floor(d.temp[0].split(':')[1]) + '℃';
        case 'wind_100':
          w = (d.wind_100[0].split(':')[1] * 1.0).toFixed(1);
          return w === 0 ? 0 : (w + '<br>km/h');
        case 'wind_10':
          w = (d.wind_10[0].split(':')[1] * 1.0).toFixed(1);
          return w === 0 ? 0 : (w + '<br>km/h');
        case 'weat':
          return Math.floor(d.weat[0].split(':')[1] * 100) + '%';
        default:
          return '';
      }
    }
  },
  getPointData: function (pos) {
    var pUrl = utils.rep('%s?timestart=%s&timeend=%s&lat=%s&lon=%s', conf.pServer, opt.testDuring[0], opt.testDuring[1], pos.lat, pos.lng);
    //var pUrl = utils.rep('%s?timestart=%s&timeend=%s&lat=%s&lon=%s', conf.pServer, utils.format(new Date(), 'full'), utils.addDate((new Date()), conf.pointDays), pos.lat, pos.lng);
    utils.mAjax(pUrl, function (res) {
      t(res);
      var data = weatherTools.format(res);
      views.point(data);
    });
  },
  updateData: function (ct) {
    var count = 2,
      hour2 = null,
      cur = null;
    var allDataSuccess = function () {
      count--;
      if (!count) {
        var mData = hour2.series;
        var opt = {
          colors: ['#068894'],
          chart: {
            type: 'areaspline',
            backgroundColor: null,
            height: 222,
            width: 295
          },
          title: {
            text: ct
          },
          subtitle: {
            text: hour2.msg,
            align: 'center',
            style: {
              color: '#068894'
            }
          },
          legend: {
            enabled: false
          },
          credits: {
            text: null
          },
          exporting: {
            enabled: false
          },
          xAxis: {
            categories: ['0', '10', '20', '30', '40', '50', '60', '70', '80', '90', '100', '110', '120'],
            tickmarkPlacement: 'on',//设置刻度线位于在类别名称的中心
            tickLength: 3,//设置刻度线的长度
            lineColor: '#068894',
            tickColor: '#068894',
            labels: {
              style: {
                color: '#000000'
              },
              step: 2//步长，每隔两个显示一次
            },
            title: {
              style: {
                color: '#000000'
              }
            }
          },
          yAxis: {
            gridLineColor: '#000000',
            gridLineDashStyle: 'longdash',
            tickPositions: [0, 2.5, 8, 16],
            labels: {
              style: {color: '#000000'}
            },
            title: {
              text: null
            },
            plotLines: [{
              value: 0,
              width: 1,
              label: {
                text: '小雨'
              }
            }, {
              value: 2.5,
              width: 1,
              label: {
                text: '中雨'
              }
            }, {
              value: 8,
              width: 1,
              label: {
                text: '大雨'
              }
            }, {
              value: 16,
              width: 1,
              label: {
                text: '暴雨'
              }
            }]
          },
          plotOptions: {
            series: {
              marker: {
                enabled: false
              }
            }
          },
          tooltip: {
            headerFormat: '第{point.key}分钟<br>',
            valueSuffix: 'mm/h',
            pointFormat: '{series.name}：{point.y}',
            backgroundColor: 'rgba(0, 0, 0, 0.2)',
            borderWidth: 0,
            shadow: false,
            style: {
              color: 'rgba(0, 0, 0, 0.8)',
              fontSize: '12px',
              fontWeight: 'normal'
            }
          }
        };
        var RainSettings = null;
        t(mData, '图表数据');
        if (mData.length) {
          RainSettings = {
            series: [{
              name: '降水',
              data: [mData[0].rain, mData[10].rain, mData[20].rain, mData[30].rain, mData[40].rain, mData[50].rain, mData[60].rain, mData[70].rain, mData[80].rain, mData[90].rain, mData[100].rain, mData[110].rain, mData[120].rain]
            }]
          };
        } else {
          RainSettings = {
            series: [{
              name: '降水',
              data: [null, null, null, null, null, null, null, null, null, null, null, null, null]
            }]
          };
        }
        $('#rainChart').highcharts($.extend({}, opt, RainSettings));
        /*当前信息*/
        $('#temp_num').html(cur.tmp);
        $('#wind_num').html(cur.wind);
        $('#wind_dir').html(cur.wdirDesc);
        $('#hum_num').html(cur.hum);
        $('#aqi_num').html(cur.aqi);
        $('#aqi_desc').html(cur.tip_aqi);
      }
    };
    utils.mAjax('http://dev.api.mlogcn.com:8000/api/weather/v2/nc/coor/' + position.getLng() + '/' + position.getLat() + '.json', function (data) {
      hour2 = data;
      allDataSuccess();
    });
    utils.mAjax('http://dev.api.mlogcn.com:8000/api/weather/v2/ob/wx/coor/' + position.getLng() + '/' + position.getLat() + '.json', function (data) {
      cur = data;
      allDataSuccess();
    });
  }
};

var mapTools = {
  mapRecenter: function (e, a) {
    map.panTo(e);
    var offsetX = a[0],
      offsetY = a[1];
    setTimeout(function () {
      map.panBy(offsetX, offsetY);
    }, 500);
  },
  mapMarker: function (pos) {
    marker.setMap(null);
    marker.setPosition(pos);
    marker.setMap(map);
    weatherTools.getPointData(pos);
    utils.reUrl(pos);
  },
  getAddress: function () {
    var lnglatXY = [position.lng, position.lat];
    var MGeocoder;
    AMap.service(['AMap.Geocoder'], function () {
      MGeocoder = new AMap.Geocoder({
        radius: 1000,
        extensions: 'all'
      });
      MGeocoder.getAddress(lnglatXY, function (status, result) {
        if (status === 'complete' && result.info === 'OK') {
          var changeCity = result.regeocode.formattedAddress;
          changeCity = changeCity.length < 9 ? changeCity : changeCity.substring(0, 9) + '...';
          weatherTools.updatePanelData(changeCity);
        }
      });
    });
  }
};
var views = {
  point: function (d) {
    if (d !== 'undefined') {
      marker.setContent('<div class="spin text-center"><span>' + d + '</span><img src="../app/images/pin.png"></div>');
      views.infoPanel();
    }
  },
  infoPanel: function () {
    mapTools.getAddress();
  },
  init: function (pos) {
    $('#infoPanel').hide();
    mapTools.mapRecenter(pos, opt.positionShift);
  }
};

var mlogMap = {
  /**
   * 将imageIndex所指向的图片显示出来,其他图片隐藏
   */
  shownImage: function () {
    mlogMap.renderTimeBar(imageIndex);
    newMapLayer = groundLayers[imageIndex];
    if (typeof newMapLayer !== 'undefined') {
      newMapLayer.setOpacity(conf.imagesOpacity);
      if (typeof oldMapLayer !== 'undefined') {
        oldMapLayer.setOpacity(0);
      }
      oldMapLayer = newMapLayer;
    }
  },
  changeImages: function () {
    imageIndex++;
    interval = config.changeSpeed;
    //是否已经运行到最后,回到开始
    if (imageIndex >= images.length) {
      imageIndex = 0;
      $('.time').css({'left': '0%'});
    }
    //gray = 1 - imageIndex / images.length;
    opacity = 0.5;
    mlogMap.shownImage();
    changeHandler = setTimeout(function () {
      mlogMap.changeImages();
    }, interval);
  },
  start: function () {
    $.when(mlogMap.changeImages()).then(function () {
      $('#play').removeClass('icon-mlogfont-play').addClass('icon-mlogfont-pause');
      pause = false;
    });
  },
  stop: function () {
    if (typeof changeHandler !== 'undefined') {
      clearTimeout(changeHandler);
    }
    $('#play').removeClass('icon-mlogfont-pause').addClass('icon-mlogfont-play');
    pause = true;
  },
  updateImages: function () {
    var imageUrl = images[images.length - 1][0];
    if (images.length > 0 && imageUrl !== '' && preImageUrl !== imageUrl) {
      for (var i = 0; i < groundLayers.length; i++) {
        groundLayers[i].setMap(null);
      }
      groundLayers.length = 0;
      timeLine = '';
      for (var image, bound, newLayer, j = 0; j < images.length; j++) {
        image = images[j][0];
        if (type.weather === 'radar') {
          bound = new AMap.Bounds(new AMap.LngLat(images[j][2][1], images[j][2][0]), new AMap.LngLat(images[j][2][3], images[j][2][2]));
        } else {
          bound = new AMap.Bounds(new AMap.LngLat(images[j][2][0].emin, images[j][2][0].nmin), new AMap.LngLat(images[j][2][0].emax, images[j][2][0].nmax));
        }
        newLayer = new AMap.GroundImage(image, bound, {map: map, clickable: true});
        newLayer.setOpacity(0);
        newLayer.setMap(map);
        groundLayers.push(newLayer);
        times.push(images[j][1]);
      }
      mlogMap.renderTimeLine(times);
      preImageUrl = imageUrl;
      mlogMap.start();
    }
  },
  requestImages: function () {
    var iUrl = conf.iServer + '?timestamp=' + utils.format(new Date()) + '&type=' + weather.type;
    utils.mAjax(iUrl, function (res) {
      var ret = $.parseJSON(res);
      var imageList = ret.series;
      if (imageList.length > 0) {
        lengths = Math.min(imageList.length, weather.length);
        for (var i = 0; i < lengths; i++) {
          images.push([conf.apiHost + imageList[i].img, imageList[i].timestamp, ret.bbox]);
        }
        mlogMap.updateImages();
      }
    });
  }
};
$(function () {
  var images = [], groundLayers = [];
  weather = weatherTools.factory('rain', 'pre');
  mapTools.mapMarker(position);
  views.init(position);
  AMap.event.addListener(map, 'complete', function () {
    mlogMap.requestImages();
    if (images.length > 0) {
      for (var i = 0; i < groundLayers.length; i++) {
        groundLayers[i].setMap(map);
      }
      mlogMap.start();
    }
  });

  AMap.event.addListener(map, 'complete', function () {
    map.plugin(['AMap.ToolBar'], function () {
      toolbar = new AMap.ToolBar({autoPosition: false});
      map.addControl(toolbar);
    });
  });

});
