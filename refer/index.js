/**
 * Created by fuchao on 15/9/2.
 */
/*global $*/
var latlon, center, location_name, zoom_scale, center_latlng, map, map_on_load = 1, center_shift = [-100, 0], ground_layers = [], images = [], image_url, images_text = "", old_images_text = "", imageIndex = 0, position, gray, pause = 0, pos_change_start = 0, slider = null, timeStr = toTimeStr(new Date()), type = 'rain', flag = 0, toolBar = null, menu = $('.l_nav'), body = $('.l_body'), opacity = 0, interval, updateInterval = 10, updateIndex = 0, t_move, weather_on_load, oldmap, timer;
//如果进行分级,则设置此处为分级分界,省级为7
var config = {
  combineZoomValue: 18,
  placeMarkerUrl: 'http',
  coordToTextUrl: 'http://',
  lonlatApiUrl: 'http:',
  combineImageUrl: 'http://',
  apiHost: 'http://123.59.79.58/',
  imagesUrl: 'ImgServer/metadata?'
};

var marker = new AMap.Marker({
  map: map,
  offset: new AMap.Pixel(-10, -34),
  icon: new AMap.Icon({
    size: new AMap.Size(65, 82),
    image: "../images/pin.png"
  }),
  zIndex: 150,
  draggable: !0
});
/*utils*/
function zeroPad(n, width) {
  var s = n.toString();
  var i = Math.max(width - s.length, 0);
  return new Array(i + 1).join("0") + s;
}

/**
 * 这里月份少算一个月的
 * @param date
 * @returns {*}
 */
function toTimeStr(date) {
  return date.getUTCFullYear() +
    zeroPad(date.getUTCMonth(), 2) +
    zeroPad(date.getUTCDate(), 2) +
    zeroPad(date.getUTCHours(), 2);
}

var initialize = function () {
  //默认放大层级
  //zoom_scale = 9;
  //设置适配
  $(document).width() <= 400 && (zoom_scale = 8);
  center_latlng = new AMap.LngLat(center[1], center[0]);
  position = center_latlng;
  //此处9不可用zoom_scale的值,因为Map闭包
  map = new AMap.Map("mapContainer", {
    center: position,
    level: 6
  });
  //placeMarker(map);
  if(type == 'cloud'){
    map.setMapStyle('dark');
  }
  map_recenter(center_latlng, center_shift);

  //底图加载完成之后,添加Layers
  AMap.event.addListener(map, "complete", function () {
    1 == map_on_load && updateData(), map_on_load = !1;
    for (var i = 0; i < ground_layers.length; i++){
      ground_layers[i].setMap(map);
    }
  });
  //底图加载完成后,添加标记点事件监听
  AMap.event.addListener(map, "complete", function () {
    map.plugin(["AMap.ToolBar"], function () {
      var toolbar = new AMap.ToolBar({autoPosition: !1});
      var offset = new AMap.Pixel(65, 82);
      toolbar.setOffset(offset);
      map.addControl(toolbar);
    });
    if (map_on_load == 1) {
      updateData();
    }
    map_on_load = !1;
    for (var i = 0; i < ground_layers.length; i++) {
      ground_layers[i].setMap(map);
    }
  });
  //添加地图点击事件
  AMap.event.addListener(map, "click", function (e) {
    position = e.lnglat;
    placeMarker(map);
  });
  AMap.event.addListener(map, "zoomchange", function () {
    updateData();
  });
  AMap.event.addListener(map, "moveend", function () {
    if (typeof map != void 0 && map.getZoom() > config.combineZoomValue) {
      loadRadarImage();
    }
  });
  gray = 1;
  $("#mapContainer").css({"-webkit-filter": " grayscale(" + gray + ")"});
  slider = $("<div id='slider'></div>").insertAfter($(".l_header_timeline")).slider({
    min: 0,
    max: 1,
    range: "min",
    value: 1,
    slide: function (e, a) {
      imageIndex = a.value,
        clearTimeout(t_move),
        showCloud(),
        $("#play").attr("data-value", "start"),
        pause = !0
    }
  });
  $("#slider").slider().mousemove(function (e) {
    if (!weather_on_load) {
      var a = $(this).width(),
        t = $(this).offset(),
        n = $(this).slider("option"),
        i = Math.round((e.clientX - t.left) / a * (n.max - n.min)) + n.min;
      0 > i && (i = 0),
      i >= n.max && (i = n.max),
        clearTimeout(t_move),
      imageIndex != i && (imageIndex = i, showCloud()),
        $("#play").attr("data-value", "start"),
        pause = !0,
        slider.slider("value", imageIndex)
    }
  }).mouseleave(function () {
    moveCloudStart()
  });
  pause = 1;
  $('#play').bind('click', function () {
    ++flag % 2 ? moveCloudPause() : moveCloudStart();
  });
  $('#forward').bind('click', function () {

  });
  $('#backword').bind('click', function () {

  });
  AMap.event.addDomListener(document.getElementById('setFitView'), 'click', function () {
    map.setFitView();
  });
  AMap.event.addDomListener(document.getElementById('toolBar'), 'click', function () {
    ++flag % 2 ? toolBar.show() : toolBar.hide();
  });
  AMap.event.addDomListener(document.getElementById('tag'), 'click', function () {
    ++flag % 2 ? placeMarker(map) : marker && marker.setMap(null);
  });
  AMap.event.addDomListener(document.getElementById('menu'), 'click', function () {
    ++flag % 2 ? menu.hide() && body.css({"marginLeft": "0"}) : menu.show() && body.css({"marginLeft": "220px"});
  });
};
/**
 * 触发地图标记,并按照地理位置取数据
 * @param map
 */
var placeMarker = function (map) {
  var now = (new Date()).getTime(), address;
  if (now - pos_change_start > 1e3) {
    marker.setMap(null);
    marker.setPosition(position);
    marker.setMap(map);
    pos_change_start = (new Date()).getTime();
    $.ajax({
      url: 'http://123.59.79.58/ImgServer/metadata?timestamp=2015082917&type=cloud',
      dataType: "json",
      complete: function (e) {
        var res = jQuery.parseJSON(e.responseText);
        try {
          address = res.address
        } catch (a) {
          address = "未知地点"
        }
      },
      error: function (e) {
        address = "";
      },
      final: function () {
        "" == address && (address = "未知地点");
        document.title = address;
        var urlLocation = document.URL.split('#')[0] + '#' + position.lng.toFixed(4) + ',' + position.lat.toFixed(4);
        history.pushState({}, "象辑科技, 气象预报", urlLocation);
        updateData();
      }
    });
  }
};
/**
 * 这种极客的写法我还是赞成的
 * @param param
 * @returns {string}
 */
var getURLParameter = function (param) {
  return decodeURI((new RegExp(param + "=(.+?)(&|$)").exec(location.search) || [, null])[1]);
};
var handleNoGeolocation = function () {
  $.ajax({
    url: "http://dev.api.mlogcn.com:8000/api/weather/v1/area/serach?area=" + location_name,
    dataType: 'json',
    complete: function (e) {
      res = jQuery.parseJSON(e.responseText);
      center = [res[4].lat, res[4].lon];
      initialize();
    }
  })
};
/**
 * 地图回到中心点
 * @param center_latlng
 * @param center_shift
 */
var map_recenter = function (center_latlng, center_shift) {
  map.panTo(center_latlng);
  setTimeout("map.panBy(center_shift[0], center_shift[1])", 500);
  updateData();
};
/**
 *
 */
var updateData = function () {
  loadCombineImage(type);
};
/**
 * 让云停住
 */
var moveCloudPause = function () {
  if (typeof t_move != "undefined") {
    clearTimeout(t_move);
    $("#play").attr("data-value", "start");
    pause = !0;
  }
};
var showCloud = function () {
  newmap = ground_layers[imageIndex], "undefined" != typeof newmap && (newmap.setOpacity(0.4), null != oldmap && oldmap.setOpacity(0), oldmap = newmap, img_time = images[imageIndex][1], $("#time").text(images[imageIndex][1]))
};
var moveCloud = function () {
  console.log(imageIndex);
  imageIndex++, interval = 400, 1 == weather_on_load && (gray = 1 - imageIndex / images.length,  $("#mapContainer").css({"-webkit-filter": " grayscale(" + gray + ")"}), interval = 400), imageIndex >= images.length && (weather_on_load = !1, imageIndex = 0, opacity = 0.4), showCloud(), slider.slider("value", imageIndex), imageIndex == images.length - 1 && (updateIndex++, updateIndex % updateInterval == 0 && updateData(), interval = 3e3), t_move = setTimeout("moveCloud()", interval)
};
var moveCloudStart = function () {
  console.log('moveCloudStart');
  moveCloud();
  $("#play").attr("data-value", "pause");
  pause = !1;
};

var loadCombineImage = function (type) {
  $.ajax({
    url: 'http://123.59.79.58/ImgServer/metadata?timestamp=2015082917&type='+type,
    dataType: "json",
    complete: function (e) {
      res = jQuery.parseJSON(e.responseText);
      if (res.series.length > 0) {
        var serie = res.series;
        for (var i = 0; i < serie.length; i++) {
          images.push(['http://123.59.79.58/'+ serie[i].img, serie[i].timestamp, res.bbox]);
        }
        //这里images 有内容了.
        updateImages();
        $("#mapContainer").css({"-webkit-filter": " grayscale(0)"});
      } else {
        $("#mapContainer").css({"-webkit-filter": " grayscale(0)"});
      }
    }
  })

};
var updateSlider = function () {
  null != slider && slider.slider({min: 0, max: images.length - 1, range: "min", value: 1});
};
var updateImages = function () {
  var already_pause = !1;
  images_text = images[images.length - 1][0];
  if (!(images.length <= 0) && ( "" != images_text && oldImageText != images_text)) {
    "start" == $("#play").attr("data-value") ? already_pause = !0 : moveCloudPause();
    for (var i = 0; i < ground_layers.length; i++) {
      ground_layers[i].setMap(null);
    }
    ground_layers.length = 0;
    for (var j = 0; j < images.length; j++) {
      var images_url = images[j][0];
      var t = new AMap.Bounds(new AMap.LngLat(images[j][2][0].emin, images[j][2][0].emax), new AMap.LngLat(images[j][2][0].nmax, images[j][2][0].nmin));
      var new_layer = new AMap.GroundImage(images_url, t, {
        map: map,
        clickable: !0
      });
      new_layer.setOpacity(0);
      new_layer.setMap(map);
      ground_layers.push(new_layer);
    }
    oldImageText = images_text;
    updateSlider();
    moveCloudStart();
  }
};
/**
 * 入口函数
 */
var getLocationThenInit = function () {
  var url = document.URL;
  if (url.split('#').length > 1) {
    latlon = url.split('#')[1];
    center = [latlon.split(',')[0], latlon.split(',')[0]];
    initialize();
  } else {
    location_name = getURLParameter('location');
    if (location_name == 'null') {
      location_name = '北京';
      handleNoGeolocation();
    }
  }
};
$(function () {
  getLocationThenInit();
});
