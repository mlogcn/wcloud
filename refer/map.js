/*global $ AMap*/
'use strict';
/**
 * 全局变量区域
 */
var position,             //存储位置的数组[116,39]
  type = 'rain',          //加载地图类型,默认为下雨
  pause = true,             //地图开始播放, 1为停止
  imageIndex = 0,         //图片数组的索引
  oldmapLayer,
  newmapLayer,
  preImageUrl,
  interval = 0,             //图片的刷新速度
  changeHandler,             //播放的handler
  toolbar = null,             //为了操作toolbar 把他放在全局
  menuSwitch = false,
  markSwitch = true,
  toolSwitch = true,
  colorBoardSwitch = false,
  images = [],              //用于存储所有的图片地址以及图片配置信息
  moveLisener,
  posChange = 0,
  opacity = 0,
  cb = $('.colorBoard'),
  demo = true,
  groundLayers = [];
/**
 * 全局配置区
 * @type {{combineZoomValue: number, apiHost: string, imageListUrl: string}}
 */
var config = {
  combineZoomValue: 18,
  center: [116.4010, 39.9814],
  changeSpeed: 1200,
  updateDuring: 800,
  restartDuring: 1e3,
  imagesOpacity: .4,
  positionShiftFix: [0, 0],
  pointDataDays: 7,
  displayPicsNo: 10,
  apiHost: 'http://123.59.79.58',
  imageListUrl: 'http://123.59.79.58/metadata',
  hackUrl: 'http://123.59.79.58/showImg',
  hackPm25: 'http://caiyunapp.com/res/storm_radar/pm25/',
  pointDataUrl: 'http://123.59.79.58/pdata'
};
/**
 * 将fetchd的配置文件添加在此处,实现图片的缓存
 * @type {{}}
 */
var myHeaders = new Headers();
myHeaders.append('Content-Type', 'application/json');
var fetchOpt = {
  method: 'GET',
  headers: myHeaders,
  mode: 'cors',
  cache: 'default'
};
var map = new AMap.Map('mapContainer', {
  resizeEnable: true,
  level: 7
});
var marker = new AMap.Marker({
  map: map,
  icon: new AMap.Icon({
    size: new AMap.Size(65, 82),
    image: './images/pin.png'
  }),
  offset: new AMap.Pixel(-30, -62),
  zIndex: 150,
  draggable: false
});
//var infoWindow = function (data) {
//
//};
/**
 * 工具集
 * @type {{zeroPad: Function, toUTCtime: Function}}
 */
var tools = {
  zeroPad: function (n, width) {
    return new Array(Math.max(width - n.toString().length, 0) + 1).join('0') + n.toString();
  },
  toUTCTimeString: function (date) {
    return demo ? '2015090102' : (date.getUTCFullYear() +
    tools.zeroPad(date.getUTCMonth() + 1, 2) +
    tools.zeroPad(date.getUTCDate(), 2) +
    tools.zeroPad(date.getUTCHours(), 2));
  },
  mapRecenter: function (e, a) {
    map.panTo(e);
    var offsetX = a[0],
      offsetY = a[1];
    setTimeout(map.panBy(offsetX, offsetY), 500);
  },
  addDate: function (start, days) {
    return demo ? '2015090402' : (tools.toUTCTimeString(new Date(start.getTime() + days * 1000 * 3600 * 24)));
  },
  sprintf: function () {
    var t = arguments, e = t[0] || '', a = 1;
    for (var i = t.length; i > a; a++) {
      e = e.replace(/%s/, t[a]);
    }
    return e;
  },
  fixRain: function (value) {
    return value === 0 ? 0 : (value + '<br>mm');
  },
  fixWind: function (value) {
    return value === 0 ? 0 : (value + '<br>km/h');
  },
  fixWeat: function (value) {
    return value === 0 ? 0 : (value + '%');
  },
  getThisData: function (d) {
    var weatherData = 'n/a';
    if (typeof d[type] !== 'undefined' && d[type].length > 0) {
      switch (type) {
        case 'rain':
          weatherData = tools.fixRain((d.rain[0].split(':')[1] * 1000).toFixed(1));
          break;
        case 'temp':
          weatherData = Math.floor(d.temp[0].split(':')[1]) + '℃';
          break;
        case 'wind_100':
          weatherData = tools.fixWind((d.wind_100[0].split(':')[1] * 1.0).toFixed(1));
          break;
        case 'wind_10':
          weatherData = tools.fixWind((d.wind_10[0].split(':')[1] * 1.0).toFixed(1));
          break;
        case 'weat':
          weatherData = tools.fixWeat(Math.floor(d.weat[0].split(':')[1] * 100));
          break;
        default:
          weatherData = 'n/a';
          break;
      }
    }
    return weatherData;
  }
};
/**
 *
 * @type {{shownImage: Function, changeImages: Function, start: Function, stop: Function, updateImages: Function, requestImages: Function, placeMarker: Function}}
 */
var mlogMap = {
  /**
   * 将imageIndex所指向的图片显示出来,其他图片隐藏
   */
  shownImage: function () {
    newmapLayer = groundLayers[imageIndex];
    if (typeof newmapLayer !== 'undefined') {
      newmapLayer.setOpacity(type === 'rain' ? opacity : config.imagesOpacity);
      if (typeof oldmapLayer !== 'undefined') {
        oldmapLayer.setOpacity(0);
      }
      oldmapLayer = newmapLayer;
      //$('#time').text(images[imageIndex][1] + images[imageIndex][0].substr(-6, 2));
      //console.log(images[imageIndex][0]);
    }
  },
  changeImages: function () {
    imageIndex++;
    interval = config.changeSpeed;
    //是否已经运行到最后,回到开始
    if (imageIndex >= images.length) {
      imageIndex = 0;
    }
    //gray = 1 - imageIndex / images.length;
    opacity = 1;
    mlogMap.shownImage();
    changeHandler = setTimeout("mlogMap.changeImages()", interval);
  },
  start: function () {
    mlogMap.changeImages();
    $('#play').removeClass('icon-mlogfont-pause').addClass('icon-mlogfont-play');
    $('.StatusPanel').html('pause: ' + pause);
    pause = false;
  },
  stop: function () {
    if (typeof changeHandler !== 'undefined') {
      clearTimeout(changeHandler);
    }
    $('#play').removeClass('icon-mlogfont-play').addClass('icon-mlogfont-pause');
    $('.StatusPanel').html('pause: ' + pause);
    pause = true;
  },
  updateImages: function () {
    var imageUrl = images[images.length - 1][0];
    if (images.length > 0 && imageUrl !== '' && preImageUrl !== imageUrl) {
      for (var i = 0; i < groundLayers.length; i++) {
        groundLayers[i].setMap(null);
      }
      groundLayers.length = 0;
      for (var image, bound, newLayer, j = 0; j < images.length; j++) {
        image = images[j][0];
        bound = new AMap.Bounds(new AMap.LngLat(images[j][2][0].emin, images[j][2][0].nmin), new AMap.LngLat(images[j][2][0].emax, images[j][2][0].nmax));
        newLayer = new AMap.GroundImage(image, bound, {map: map, clickable: true});
        newLayer.setOpacity(0);
        newLayer.setMap(map);
        groundLayers.push(newLayer);
      }
      preImageUrl = imageUrl;
      mlogMap.start();
    }
  },
  updateCyImages: function () {
    var imagesText = images[images.length - 1][0];
    if (!(images.length <= 0) && (imagesText !== '' && preImageUrl !== imagesText)) {
      for (var i = 0; i < groundLayers.length; i++) {
        groundLayers[i].setMap(null);
      }
      for (groundLayers.length = 0, i = 0; i < images.length; i++) {
        var image = images[i][0].replace('clean', 'cleansharp');
        var e = new AMap.LngLat(images[i][2][1], images[i][2][0]), a = new AMap.LngLat(images[i][2][3], images[i][2][2]), t = new AMap.Bounds(e, a);
        var newLayer = new AMap.GroundImage(image, t, {
          map: map,
          clickable: !0
        });
        newLayer.setOpacity(0);
        newLayer.setMap(map);
        groundLayers.push(newLayer);
      }
      preImageUrl = imagesText;
      mlogMap.start();
    }
  },
  requestImages: function () {
    if (type === 'rain') {
      //彩云数据
      window.fetch(config.hackUrl).then(function (req) {
        if (req.ok) {
          req.json().then(function (res) {
            if (res.status === 'ok') {
              if ('radar_img' in res) {
                images = res.radar_img;
                console.log('图层覆盖:', type, images);
                mlogMap.updateCyImages();
              } else {
                $('#mapContainer').css({'-webkit-filter': ' grayscale(0)'});
              }
            }
          });
        } else {
          console.log('this is the status while there dont have the data', req.status);
        }
      }, function (e) {
        console.log('request error', e);
      });
    } else if( type === 'air'){
      window.fetch(config.hackUrl).then(function (req) {
        if (req.ok) {
          req.json().then(function (res) {
            if (res.status === 'ok') {
              if ('radar_img' in res) {
                images = res.radar_img;
                console.log('图层覆盖:', type, images);
                mlogMap.updateCyImages();
              } else {
                $('#mapContainer').css({'-webkit-filter': ' grayscale(0)'});
              }
            }
          });
        } else {
          console.log('this is the status while there dont have the data', req.status);
        }
      }, function (e) {
        console.log('request error', e);
      });
    }else {
      //我们的图
      var requestUrl = config.imageListUrl + '?timestamp=' + tools.toUTCTimeString(new Date()) + '&type=' + type;
      window.fetch(requestUrl, fetchOpt).then(function (req) {
        if (req.ok) {
          req.json().then(function (ret) {
            var imageList = ret.series;
            if (imageList.length > 0) {
              for (var i = 0; i < config.displayPicsNo & demo && imageList.length; i++) {
                //var imgUrl = 'config.apiHost + '/' + imageList[i].img';
                var imgUrl = '../images/demo.png';
                images.push([imgUrl, imageList[i].timestamp, ret.bbox]);
              }
              console.log('图层覆盖', type, images);
              mlogMap.updateImages();
            }
          });
        } else {
          $('#mapContainer').css({'-webkit-filter': ' grayscale(0)'});
          console.log('this is the status while there dont have the data', req.status);
        }
      }, function (e) {
        $('#mapContainer').css({'-webkit-filter': ' grayscale(0)'});
        console.log('request error', e);
      });
    }
  },
  /**
   * 向某点添加地址信息
   * @param e
   */
  placeMarker: function (e) {
    moveLisener = $.now();
    if (moveLisener - posChange >= 1e3) {
      marker.setMap(null);
      marker.setPosition(position);
      marker.setMap(e);
      posChange = $.now();
      // 按照点的坐标,取对应的数据值
      var pUrl = tools.sprintf('%s?timestart=%s&timeend=%s&lat=%s&lon=%s', config.pointDataUrl, tools.toUTCTimeString(new Date()), +tools.addDate((new Date()), config.pointDataDays), position.getLat(), position.getLng());
      window.fetch(pUrl, fetchOpt).then(function (res) {
        if (res.ok) {
          res.json().then(function (data) {
            var datas = tools.getThisData(data);
            console.log('单点取值', datas, pUrl);
            if (datas !== 'undefined') {
              marker.setContent('<div class="spin text-center"><span>' + datas + '</span><img src="../app/images/pin.png"></div>');
              marker.setExtData(data);
            }
          });
        } else {
          console.log('this is the status while there dont have the data', res.status);
        }
      }, function (err) {
        console.log('fetch error', err);
      });
      var urlBase = document.URL.split('#')[0];
      var formatedUrl = urlBase + '#' + position.lng.toFixed(4) + ',' + position.lat.toFixed(4);
      history.pushState({}, config.urlDescription, formatedUrl);
      //mlogMap.requestData();
    }
  },
  /**
   * 逐步向后播放图片
   */
  forwordByStep: function () {

  },
  /**
   * 逐步向前播放图片
   */
  backwordByStep: function () {

  },
  /**
   * 显示时间轴
   */
  showLine: function () {

  }
};
/**
 * 项目初始化
 */
var initialize = function () {
  //初始化的位置在中心点位置
  position = new AMap.LngLat(config.center[0], config.center[1]);
  mlogMap.placeMarker(map);
  tools.mapRecenter(position, config.positionShiftFix);
  //当地图的瓦片底图加载完毕后,加载所有的底图
  AMap.event.addListener(map, 'complete', function () {
    mlogMap.requestImages();
    if (images.length > 0) {
      for (var i = 0; i < groundLayers.length; i++) {
        groundLayers[i].setMap(map);
      }
      mlogMap.start();
    }
  });
  AMap.event.addListener(map, 'complete', function () {
    map.plugin(['AMap.ToolBar'], function () {
      toolbar = new AMap.ToolBar({autoPosition: false});
      map.addControl(toolbar);
    });
    $('.infoPanel').hide();
  });
  AMap.event.addListener(map, 'click', function (pos) {
    position = pos.lnglat;
    mlogMap.placeMarker(map);
    $('.infoPanel').hide();
  });
  AMap.event.addListener(map, 'zoomchange', function () {
    mlogMap.requestImages();
  });
  AMap.event.addListener(marker, 'click', function () {
    //获取到单点的数据this.getExtData()
    console.log(this.getPosition());
    $('.infoPanel').css({});
    $('.infoPanel').show();
  });
  $('.l_nav').find('input').bind('click', function () {
    type = this.id;
    mlogMap.stop();
    map.clearMap();
    console.log('加载地图类型', type);
    if (type === 'wind_10s') {
      $('#mapContainer').hide();
      $('.l_body').append('<iframe scrolling="no" width="100%" height="100%" src="../bower_components/wind10s/example/map3d_wave.html"></iframe>');
    } else {
      $('iframe').remove();
      $('#mapContainer').show();
      for (var i = 0; i < groundLayers.length; i++) {
        groundLayers[i].setMap(null);
      }
      groundLayers.length = 0;
      images = [];
      if (type === 'cloud' || type === 'cloud_low') {
        map.setMapStyle('dark');
      } else {
        map.setMapStyle('normal');
      }
      cb.empty();
      cb.append('<div id="color_' + type + '_outer"><div id="color_' + type + '"></div></div>').show();
      mlogMap.requestImages();
      mlogMap.placeMarker(map);
    }
  });
  //DOM 事件响应
  $('#play').bind('click', function () {
    if (pause === false) {
      mlogMap.stop();
    } else {
      mlogMap.start();
    }
  });
  $('#forward').bind('click', function () {
    mlogMap.forwordByStep();
  });
  $('#backword').bind('click', function () {
    mlogMap.backwordByStep();
  });
  $('#setFitView').bind('click', function () {
    map.setFitView();
  });
  $('#toolBar').bind('click', function () {
    if (toolSwitch) {
      toolbar.show();
    } else {
      toolbar.hide();
    }
    toolSwitch = !toolSwitch;
  });
  $('#tag').bind('click', function () {
    if (markSwitch) {
      marker.show();
    } else {
      marker.hide();
    }
    markSwitch = !markSwitch;
  });
  $('#colorBoard').bind('click', function () {
    if (colorBoardSwitch) {
      cb.empty();
      cb.append('<div id="color_' + type + '_outer"><div id="color_' + type + '"></div></div>').show();
    } else {
      cb.empty().hide();
    }
    colorBoardSwitch = !colorBoardSwitch;
  });
  $('#menu').bind('click', function () {
    var menu = $('.l_nav'), body = $('.l_body');
    if (menuSwitch) {
      menu.hide();
      body.css({'marginLeft': '0'});
    } else {
      menu.show();
      body.css({'marginLeft': '220px'});
    }
    menuSwitch = !menuSwitch;
  });
};
$(function () {
  //todo 添加定位函数,为初始化添加初始位置<高德地图默认初始化在用户打开区域>
  initialize();
});
