function preLoadImg(e) {
  var a = new Image;
  a.src = e
}
function getURLParameter(e) {
  return decodeURI((RegExp(e + "=(.+?)(&|$)").exec(location.search) || [, null])[1])
}
function updateImages() {
  if (!(images.length <= 0) && (images_text = images[images.length - 1][0], "" != images_text && old_images_text != images_text)) {
    for (already_pause = !1, "images/start.png" == $("#switch").attr("src") ? already_pause = !0 : moveCloudPause(), i = 0; i < ground_layers.length; i++)ground_layers[i].setMap(null);
    for (ground_layers.length = 0, i = 0; i < images.length; i++) {
      images_url = images[i][0].replace("clean", "cleansharp");
      var e = new AMap.LngLat(images[i][2][1], images[i][2][0]), a = new AMap.LngLat(images[i][2][3], images[i][2][2]), t = new AMap.Bounds(e, a);
      new_layer = new AMap.GroundImage(images_url, t, {
        map: map,
        clickable: !0
      }), new_layer.setOpacity(0), new_layer.setMap(map), ground_layers.push(new_layer)
    }
    old_images_text = images_text, updateSlider(), already_pause || moveCloudStart()
  }
}
function moveCloudPause() {
  "undefined" != typeof t_move && clearTimeout(t_move), $("#switch").attr("src", "images/start.png"), pause = !0
}
function moveCloudStart() {
  moveCloud(), $("#switch").attr("src", "images/pause.png"), pause = !1
}
function getStationInfoThenInit() {
  var e = document.URL;
  return e.split("#").length > 1 ? (latlon = e.split("#")[1], lon = latlon.split(",")[0], lat = latlon.split(",")[1], center = [lat, lon], void initialize()) : (station_name = getURLParameter("station"), "null" == station_name && (station_name = "beijing"), void handleNoGeolocation(!0))
}
function initialize() {
  zoom_scale = 9, $(document).width() <= 400 && (zoom_scale = 8);
  center_latlng = new AMap.LngLat(center[1], center[0]), position = center_latlng, map = new AMap.Map("map_canvas", {
    center: position,
    level: 9
  }), placeMarker(map), map_recenter(center_latlng, center_shift), AMap.event.addListener(map, "complete", function () {
    for (1 == map_on_load && updateData(), map_on_load = !1, i = 0; i < ground_layers.length; i++)ground_layers[i].setMap(map)
  }), AMap.event.addListener(map, "complete", function () {
    for (map.plugin(["AMap.ToolBar"], function () {
      var e = new AMap.ToolBar({autoPosition: !1}), a = new AMap.Pixel(10, 60);
      e.setOffset(a), map.addControl(e)
    }), 1 == map_on_load && ($("#TopTipHolder").hide(), updateData()), map_on_load = !1, i = 0; i < ground_layers.length; i++)ground_layers[i].setMap(map)
  }), AMap.event.addListener(map, "click", function (e) {
    position = e.lnglat, placeMarker(map)
  }), AMap.event.addListener(map, "zoomchange", function (e) {
    updateData()
  }), AMap.event.addListener(map, "moveend", function (e) {
    void 0 != typeof map && map.getZoom() > combineZoomValue && loadRadarImage()
  }), gray = 1, $("#map_canvas").css({"-webkit-filter": " grayscale(" + gray + ")"}), slider = $("<div id='slider'></div>").insertAfter($("#time")).slider({
    min: 0,
    max: 1,
    range: "min",
    value: 1,
    slide: function (e, a) {
      imageIndex = a.value, clearTimeout(t_move), showCloud(), $("#switch").attr("src", "images/start.png"), pause = !0
    }
  }), $("#slider").slider().mousemove(function (e) {
    if (!weather_on_load) {
      var a = $(this).width(), t = $(this).offset(), n = $(this).slider("option"), i = Math.round((e.clientX - t.left) / a * (n.max - n.min)) + n.min;
      0 > i && (i = 0), i >= n.max && (i = n.max), clearTimeout(t_move), imageIndex != i && (imageIndex = i, showCloud()), $("#switch").attr("src", "images/start.png"), pause = !0, slider.slider("value", imageIndex)
    }
  }).mouseleave(function () {
    moveCloudStart()
  }), pause = !1, $("#switch").bind("click", function () {
    0 == pause ? moveCloudPause() : moveCloudStart()
  }), $("#overview").draggable({
    containment: "parent",
    handel: "section.currently.section"
  }), $("input.location_field").bind("webkitspeechchange", function () {
    updateTextAddress(this, map)
  }), $("input.location_field").bind("keypress", function (e) {
    13 == e.keyCode && updateTextAddress(this, map)
  });
  var e = (new Date).getTime();
  draw_canvas($("#wobble_container>canvas")[0], e)
}
function map_recenter(e, a) {
  map.panTo(e), offsetx = a[0], offsety = a[1], setTimeout("map.panBy(offsetx,offsety)", 500)
}
function updateTextAddress(e, a) {
  $("#overview").css({opacity: .5}), $.ajax({
    url: "/fcgi-bin/v1/text2coord.py?address=" + e.value + "&random=" + Math.random(),
    dataType: "json",
    complete: function (e) {
      res = jQuery.parseJSON(e.responseText), position = new AMap.LngLat(res.coord.lng, res.coord.lat), placeMarker(a), map_recenter(position, center_shift)
    },
    error: function () {
      alert("哎呀，地图君忘了" + e.value + "在哪儿了！我们等下再问问他？"), $("#overview").css({opacity: 1})
    }
  })
}
function handleNoGeolocation(e) {
  if (e); else;
  $.ajax({
    url: "/fcgi-bin/v1/geoip.py", dataType: "json", complete: function (e) {
      res = jQuery.parseJSON(e.responseText), center = res.center, initialize()
    }
  })
}
function draw_canvas(e, a) {
  if (void 0 != rainfall_data) {
    var t = (new Date).getTime() - a, n = Array.apply(null, new Array(rainfall_data.length)).map(Number.prototype.valueOf, 0), o = n.length, r = e.width, s = e.height, l = e.getContext("2d");
    for (speed = 1.1, wave_length = 6, wave_size = .4, i = 0; i < o; i++)speed = 1.1, wave_length = 6, wave_size = 8, n[i] = rainfall_data[i] + wave_size * Math.sin(i / o * wave_length * Math.PI * 3 + 2 * Math.PI * t / (1e3 * speed)) / s / 10, speed = 1.3, wave_length = 4, wave_size = 6, n[i] = rainfall_data[i] + wave_size * Math.cos(i / o * wave_length * Math.PI * 3 - 3.2 * Math.PI * t / (1e3 * speed)) / s / 10;
    l.clearRect(0, 0, r, s);
    var d = r / (o - 1);
    for (i = 0; i < o - 1; i++)l.fillStyle = "#1878f0", l.beginPath(), l.moveTo(i * d, s), l.lineTo(i * d, s * (1 - n[i])), l.lineTo((i + 1.2) * d, s * (1 - n[i + 1])), l.lineTo((i + 1.2) * d, s), l.lineTo(i * d, s), l.closePath(), l.fill()
  }
  requestAnimFrame(function () {
    draw_canvas(e, a)
  })
}
function formatDate(e) {
  return prefix = "", hour = e.getHours(), hour < 6 ? prefix = "凌晨" : hour < 9 ? prefix = "早上" : hour < 12 ? prefix = "上午" : hour < 14 ? prefix = "中午" : hour < 17 ? prefix = "下午" : hour < 19 ? prefix = "傍晚" : hour < 22 ? prefix = "晚上" : prefix = "夜里", hour > 12 && (hour -= 12), min = e.getMinutes(), hour = checkTime(hour), min = checkTime(min), prefix + " " + hour + ":" + min
}
function checkTime(e) {
  return 10 > e && (e = "0" + e), e
}
function loadCombineImage() {
  $.ajax({
    url: "/fcgi-bin/v1/img.py?token=96Ly7wgKGq6FhllM", dataType: "json", complete: function (e) {
      res = jQuery.parseJSON(e.responseText), "ok" == res.status && ("radar_img"in res ? (images = res.radar_img, updateImages()) : $("#map_canvas").css({"-webkit-filter": " grayscale(0)"}))
    }
  })
}
function loadRadarImage() {
  var e = map.getCenter().getLng(), a = map.getCenter().getLat();
  $.ajax({
    url: "/fcgi-bin/v1/api.py?lonlat=" + e + "," + a + "&format=json&product=minutes_prec&token=96Ly7wgKGq6FhllM&random=" + Math.random(),
    dataType: "json",
    complete: function (e) {
      res = jQuery.parseJSON(e.responseText), "ok" == res.status && "radar_img"in res && map.getZoom() > combineZoomValue ? (images = res.radar_img, updateImages()) : $("#map_canvas").css({"-webkit-filter": " grayscale(0)"})
    }
  })
}
function updateData() {
  $.ajax({
    url: "/fcgi-bin/v1/api.py?lonlat=" + position.lng + "," + position.lat + "&format=json&product=minutes_prec&token=96Ly7wgKGq6FhllM&random=" + Math.random(),
    dataType: "json",
    complete: function (request) {
      res = jQuery.parseJSON(request.responseText), $("section.currently>div>div>div>div.desc").text(res.descript_now), temp = res.temp, -273 == temp ? $("div.current_container").hide() : $("div.current_container").show(), $(".temp>span").text(temp + "°"), skycon_now = res.skycon, icons.set("icon_current", eval("Skycons." + skycon_now)), icons.play(), $("h2#address").text(formatDate(new Date(1e3 * res.server_time))), "ok" == res.status ? ($("section.next_hour>div>div.desc").text(res.summary), -1 != res.summary.indexOf("雪") ? ($(".intensity_labels>.heavy>span").text("大雪"), $(".intensity_labels>.med>span").text("中雪"), $(".intensity_labels>.light>span").text("小雪")) : ($(".intensity_labels>.heavy>span").text("大雨"), $(".intensity_labels>.med>span").text("中雨"), $(".intensity_labels>.light>span").text("小雨")), rainfall_data = res.dataseries, $("#wobble_container").show()) : ($("#wobble_container").hide(), "summary"in res ? $("section.next_hour>div>div.desc").text(res.summary) : -1 != res.error_type[0].indexOf("outside_station") ? $("section.next_hour>div>div.desc").text("当前区域不在雷达站范围内，无法提供一小时详细降雨预报。") : -1 != res.error_type[0].indexOf("too_old") ? $("section.next_hour>div>div.desc").text("气象雷达停机中，无法提供一小时详细降雨预报。") : -1 != res.error_type[0].indexOf("too_sparse") && $("section.next_hour>div>div.desc").text("气象雷达数据累积中，暂时无法提供一小时详细降雨预报。")), $("#overview").css({opacity: 1}), map.getZoom() <= combineZoomValue ? loadCombineImage() : "radar_img"in res && map.getZoom() > combineZoomValue ? (images = res.radar_img, updateImages()) : $("#map_canvas").css({"-webkit-filter": " grayscale(0)"})
    }
  })
}
function placeMarker(e) {
  now = (new Date).getTime(), now - pos_change_start < 1e3 || ($("#overview").css({opacity: .5}), marker.setMap(null), marker.setPosition(position), marker.setMap(e), pos_change_start = (new Date).getTime(), $.ajax({
    url: "/fcgi-bin/v1/coord2text.py?latlng=" + position.lat + "," + position.lng + "&random=" + Math.random(),
    dataType: "json",
    complete: function (e) {
      res = jQuery.parseJSON(e.responseText);
      try {
        address = res.address
      } catch (a) {
        address = "未知地点"
      }
      "" == address && (address = "未知地点"), $("input.location_field").val(address), $("#overview").css({opacity: 1});
      var t = "彩云天气 分钟预报", n = {}, i = document.URL;
      i = i.split("#")[0];
      var o = i + "#" + position.lng.toFixed(4) + "," + position.lat.toFixed(4);
      history.pushState(n, t, o), updateData()
    },
    error: function () {
      address = "未知地点", $("input.location_field").val(address), $("#overview").css({opacity: 1});
      var e = "彩云天气 分钟预报", a = {}, t = document.URL;
      t = t.split("#")[0];
      var n = t + "#" + position.lng.toFixed(4) + "," + position.lat.toFixed(4);
      history.pushState(a, e, n), updateData()
    }
  }))
}
function showCloud() {
  newmap = ground_layers[imageIndex], "undefined" != typeof newmap && (newmap.setOpacity(opacity), null != oldmap && oldmap.setOpacity(0), oldmap = newmap, img_time = images[imageIndex][1], $("#time").text(formatDate(new Date(1e3 * img_time))))
}
function moveCloud() {
  imageIndex++, interval = 40, 1 == weather_on_load && (gray = 1 - imageIndex / images.length, opacity = 1 - gray, $("#map_canvas").css({"-webkit-filter": " grayscale(" + gray + ")"}), $("#panel").css({opacity: .8 * opacity}), interval = 40), imageIndex >= images.length && (weather_on_load = !1, imageIndex = 0, opacity = 1), showCloud(), slider.slider("value", imageIndex), imageIndex == images.length - 1 && (updateIndex++, updateIndex % updateInterval == 0 && updateData(), interval = 3e3), t_move = setTimeout("moveCloud()", interval)
}
function updateSlider() {
  null != slider && slider.slider({min: 0, max: images.length - 1, range: "min", value: 1})
}
var map, oldmap, station_id = "AZ9010", center_shift = [-100, 0], position, delta_lat, delta_long, rainfall_data, pause = !1, imageBounds, icons = new Skycons, marker = new AMap.Marker({
  map: map,
  position: new AMap.LngLat(116.406326, 39.903942),
  offset: new AMap.Pixel(-10, -34),
  icon: "http://webapi.amap.com/images/0.png",
  zIndex: 150
}), geocoder;
pos_change_start = 0;
var combineZoomValue = 7;
images = [], ground_layers = [], images_text = "", old_images_text = "", imageIndex = 0, slider = null, weather_on_load = !0, map_on_load = !0, window.requestAnimFrame = function (e) {
  return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function (e) {
      window.setTimeout(e, 1e3 / 60)
    }
}(), updateInterval = 10, updateIndex = 0;
