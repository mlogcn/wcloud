/*global $*/
$(function () {
  'use strict';
});

  /*utils*/
  function zeroPad(n, width) {
    var s = n.toString();
    var i = Math.max(width - s.length, 0);
    return new Array(i + 1).join("0") + s;
  }

  /**
   * 这里月份少算一个月的
   * @param date
   * @returns {*}
   */
  function toTimeStr(date) {
    return date.getUTCFullYear() +
      zeroPad(date.getUTCMonth(), 2) +
      zeroPad(date.getUTCDate(), 2) +
      zeroPad(date.getUTCHours(), 2);
  }
  /*definitions*/
  /**
   * 更新这个数据结构来完成对图片的刷新
   * @type {{source: string, opacity: number, bounder: number[]}}
   */
  var latlon, center, location_name, zoom_scale, center_latlng, map, map_on_load, center_shift, ground_layers =[], images = [], image_url, images_text = "", old_images_text = "", imageIndex = 0, position, gray, pause, pos_change_start, slider=null;
  var config = {
    apiHost: 'http://123.59.79.58',
    imagesUrl: '/ImgServer/metadata?'
  };
  var src = {
    source: '../images/03.png',
    opacity: 0.4,
    bounder: [68.9870452481536, 0.9584302557704305, 137.8067547518464, 60.8573697442295]
  };
  var imageLayer = new AMap.ImageLayer({
    url: src.source,
    opacity: 0.4,
    bounds: new AMap.Bounds(
      [src.bounder[0], src.bounder[1]],
      [src.bounder[2], src.bounder[3]]
    ),
    zooms: [4, 15]
  });
  var map = new AMap.Map('mapContainer', {
    resizeEnable: true,
    zooms: [5, 14],
    layers: [
      new AMap.TileLayer(),
      imageLayer
    ]
  });
  var markers = [{
    position: [73.9870452481536, 16.9584302557704305]
  }, {
    position: [134.8067547518464, 53.85736974422957]
  }, {
    position: [73.9870452481536, 53.85736974422957]
  }];
  var timeStr = toTimeStr(new Date()), tagMarker, flag = 0, toolBar = null, menu = $('.l_nav'), body = $('.l_body');

  function addMarker() {
    tagMarker = new AMap.Marker({
      icon: new AMap.Icon({
        size: new AMap.Size(65, 82),
        image: "../images/pin.png"
      }),
      position: map.getCenter(),
      draggable: true,
      cursor: 'move',
      raiseOnDrag: true
    });
    tagMarker.setMap(map);
    tagMarker.setLabel({content: '  test'});
  }

var initMap = function () {
  map.plugin(["AMap.ToolBar"], function () {
    toolBar = new AMap.ToolBar();
    map.addControl(toolBar);
  });
  map.plugin(["AMap.Scale"], function () {
    var scale = new AMap.Scale();
    map.addControl(scale);
    scale.show();
  });
  markers.forEach(function (marker) {
    new AMap.Marker({
      map: map,
      position: [marker.position[0], marker.position[1]]
    });
  });
  AMap.event.addDomListener(document.getElementById('setFitView'), 'click', function () {
    map.setFitView();
  });
  AMap.event.addDomListener(document.getElementById('toolBar'), 'click', function () {
    ++flag % 2 ? toolBar.show() : toolBar.hide();
  });
  AMap.event.addDomListener(document.getElementById('tag'), 'click', function () {
    ++flag % 2 ? addMarker() : tagMarker && tagMarker.setMap(null);
  });
  AMap.event.addDomListener(document.getElementById('menu'), 'click', function () {
    ++flag % 2 ? menu.hide() && body.css({"marginLeft": "0"}) : menu.show() && body.css({"marginLeft": "220px"});
  });
  //http://123.59.79.58/ImgServer/metadata?timestamp=2015082917&type=cloud
  $('#menuContent').find('input').on('click', function () {
    getIndex(this.id);
  });
  $('#play').on('click', function () {
    console.log('play');
    src.source = '../images/03.png';
    map.setLayers([
      imageLayer
    ])
  });
  $('#forward').on('click', function () {
    console.log('forword');
  });
  $('#backword').on('click', function () {
    console.log('backword');
  });
};



